package u03

import org.junit.*
import org.junit.Assert.*
import Lists.*
import u02.Modules.Person

class ListTest:
  import List.*

  val l: List[Int] = Cons(10, Cons(20, Cons(30, Nil())))

  @Test def testSum(): Unit =
    assertEquals(0, sum(Nil()))
    assertEquals(60, sum(l))

  @Test def testMap(): Unit =
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), map(l)(_ + 1))
    assertEquals(Cons("10", Cons("20", Cons("30", Nil()))), map(l)(_ + ""))

  @Test def testFilter(): Unit =
    assertEquals(Cons(20, Cons(30, Nil())), filter(l)(_ >= 20))
    assertEquals(Cons(10, Cons(30, Nil())), filter(l)(_ != 20))

  @Test def testDrop(): Unit =
    assertEquals(Cons(20, Cons(30, Nil())), drop(l,1))
    assertEquals(Cons(30, Nil()), drop(l,2))
    assertEquals(Nil(), drop(l,5))

  @Test def testAppend(): Unit =
    assertEquals(Cons(10,Cons(20,Cons(30,Cons(40,Nil())))),append(l,Cons(40,Nil())))
    assertEquals(Cons(10,Cons(20,Cons(30,Cons(40,Cons(50,Nil()))))),append(l,Cons(40,Cons(50,Nil()))))
    assertEquals(Cons(10,Cons(20,Cons(30,Nil()))),append(l,Nil()))

  @Test def testFlatMap(): Unit =
    assertEquals(Cons(11,Cons(21,(Cons(31,Nil())))),flatMap(l)(v=>Cons(v+1,Nil())))
    assertEquals(Cons(11,Cons(12,Cons(21,Cons(22,(Cons(31,Cons(32,Nil()))))))),flatMap(l)(v=>Cons(v+1,Cons(v+2,Nil()))))
    
  @Test def testMapTwo(): Unit =
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), mapTwo(l)(_ + 1))
    assertEquals(Cons("10", Cons("20", Cons("30", Nil()))), mapTwo(l)(_ + ""))

  // Non passato
  @Test def testFilterTwo(): Unit =
    assertEquals(Cons(20, Cons(30, Nil())), filterTwo(l)(_ >= 20))
    assertEquals(Cons(10, Cons(30, Nil())), filterTwo(l)(_ != 20))

  // Non passato
  @Test def testMax(): Unit =
    assertEquals(30, max(Cons(10,Cons(20,Cons(30,Nil())))))
    assertEquals(25, max(Cons(10,Cons(25,Cons(15,Nil())))))
    assertEquals(75, max(Cons(75,Cons(25,Cons(15,Cons(40,Cons(15,Nil())))))))

  // Non passato
  @Test def testGetCourses(): Unit =
    val people: List[Person] = List.Cons(Person.Teacher("Mario","Reti"),Cons(Person.Student("Luca",2021),Cons(Person.Teacher("Francesco","Programmazione"), Nil())))
    assertEquals(List.Cons("Reti",Cons("Programmazione",Nil())),getCourses(people))

  // Non passato
  @Test def testFoldLeft(): Unit =
    val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    assertEquals(-16, foldLeft(lst)(0)(_ - _))

  // Non passato
  @Test def testFoldRight(): Unit =
    val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    assertEquals(-8, foldRight(lst)(0)(_ - _))



