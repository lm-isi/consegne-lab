package u03

import org.junit.*
import org.junit.Assert.*
import u03.Streams.*
import u03.Lists.*
import Streams.*

class StreamsTest:
  import List.{Cons,Nil}
  import Stream.*

  val l: List[Int] = Cons(10, Cons(20, Cons(30, Nil())))

  @Test def testDrop(): Unit =
    val elementsTaken = 10
    val s = take(iterate(0)(_+1))(elementsTaken)
    assertEquals(Cons(6,Cons(7,Cons(8,Cons(9,Nil())))),toList(drop(s)(6)))
    assertEquals(Nil(),toList(drop(s)(elementsTaken)))
    assertEquals(toList(s),toList(drop(s)(0)))

  @Test def testConstant(): Unit =
    var firstElement = 5
    var elementsNumber = 7
    assertEquals(toList(take(iterate(firstElement)(v=>v))(elementsNumber)),toList(take(constant(firstElement))(elementsNumber)))
    firstElement = 1
    elementsNumber = 9
    assertEquals(toList(take(iterate(firstElement)(v=>v))(elementsNumber)),toList(take(constant(firstElement))(elementsNumber)))

  @Test def testFibs(): Unit =
    assertEquals(Cons(0,Cons(1,Nil())),toList(take(fibs)(2)))
    assertEquals(Cons(0,Cons(1,Cons(1,Cons(2,Nil())))),toList(take(fibs)(4)))
    assertEquals(Cons(0,Cons(1,Cons(1,Cons(2,Cons(3,Cons(5,Cons(8,Cons(13,Nil())))))))),toList(take(fibs)(8)))





    

