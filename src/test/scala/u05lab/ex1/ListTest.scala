package u05lab.ex1

import org.junit.Assert.*
import org.junit.Test

class ListTest:

  /** todo */
  @Test
  def testZipRight(): Unit =
    val reference = List(10,20,30,40)
    assertEquals(List((10,0),(20,1),(30,2),(40,3)),reference.zipRight)

  @Test
  def testPartition(): Unit =
    val reference = List(1,2,3,4)
    assertEquals((List(2,4),List(1,3)),reference.partition(_ % 2 == 0))
    assertEquals((List(1,2,3,4),List.Nil()),reference.partition(_ % 1 == 0))

  @Test
  def testSpan(): Unit =
    val reference = List(1,2,3,4,5,6)
    assertEquals((List(1),List(2,3,4,5,6)),reference.span(_ < 2))
    assertEquals((List(1),List(2,3,4,5,6)),reference.span(_ % 2 != 0))
    assertEquals((List(1,2,3),List(4,5,6)),reference.span(_<=3))

  @Test
  def testReduce(): Unit =
    var reference = List(1,2,3,4)
    var reduceExptected = 10
    assertEquals(reduceExptected,reference.reduce(_+_))
    reference = List(9,9,9,9)
    reduceExptected = 9*4
    assertEquals(reduceExptected,reference.reduce(_+_))

  @Test
  def testReduce2(): Unit =
    var reference = List(1,2,3,4)
    var reduceExptected = 10
    assertEquals(reduceExptected,reference.reduce2(_+_))
    reference = List(9,9,9,9)
    reduceExptected = 9*4
    assertEquals(reduceExptected,reference.reduce2(_+_))

  @Test
  def testReduceWithEmptyList(): Unit =
    assertThrows(classOf[UnsupportedOperationException], () => List.Nil().reduce((a,b)=>a))

  @Test
  def testReduce2WithEmptyList(): Unit =
    assertThrows(classOf[UnsupportedOperationException], () => List.Nil().reduce2((a,b)=>a))

  @Test
  def testTakeRight(): Unit =
    val reference = List(1,2,3,4,5,6,7,8,9)
    assertEquals(List(9),reference.takeRight(1))
    assertEquals(List.Nil(),reference.takeRight(0))
    assertEquals(List(7,8,9),reference.takeRight(3))

  @Test
  def testTakeRight2(): Unit =
    val reference = List(1,2,3,4,5,6,7,8,9)
    assertEquals(List(9),reference.takeRight2(1))
    assertEquals(List.Nil(),reference.takeRight2(0))
    assertEquals(List(7,8,9),reference.takeRight2(3))

  @Test
  def testCollect(): Unit =
    val reference = List(1,2,3,4,5,6)
    val pf1: PartialFunction[Int, Int] =
      case x if (x % 2) == 0 => 10*x
    val pf2: PartialFunction[Int, Int] =
      case x if (x % 2) != 0 => 20*x
    val pf3: PartialFunction[Int, Int] =
      case x if (x <= 3) => 10*x
    assertEquals(List(20,40,60),reference.collect(pf1))
    assertEquals(List(20,60,100),reference.collect(pf2))
    assertEquals(List(10,20,30),reference.collect(pf3))

  @Test
  def testCollect2(): Unit =
    val reference = List(1,2,3,4,5,6)
    val pf1: PartialFunction[Int, Int] =
      case x if (x % 2) == 0 => 10*x
    val pf2: PartialFunction[Int, Int] =
      case x if (x % 2) != 0 => 20*x
    val pf3: PartialFunction[Int, Int] =
      case x if (x <= 3) => 10*x
    assertEquals(List(20,40,60),reference.collect2(pf1))
    assertEquals(List(20,60,100),reference.collect2(pf2))
    assertEquals(List(10,20,30),reference.collect2(pf3))







