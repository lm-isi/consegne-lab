package u03

import scala.annotation.tailrec

object Lists extends App:
  import Option.*
  import u02.Modules.*

  // A generic linkedlist
  enum List[E]:
    case Cons(head: E, tail: List[E])
    case Nil()
  // a companion object (i.e., module) for List
  object List:

    def sum(l: List[Int]): Int = l match
      case Cons(h, t) => h + sum(t)
      case _ => 0

    def map[A, B](l: List[A])(mapper: A => B): List[B] = l match
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match
      case Cons(h, t) if pred(h) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()

    @tailrec
    def drop[A](l: List[A], n: Int): List[A] = l match
      case Cons(head, tail) if n == 0 => l
      case Cons(head, tail) => drop(tail,n-1)
      case _ => Nil()

    def append[A](left: List[A], right: List[A]): List[A] = left match
      case Cons(head,tail) => Cons(head, append(tail, right))
      case _ => right

    def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match
      case Cons(head,tail) => append(f(head), flatMap(tail)(f))
      case _ => Nil()

    def mapTwo[A, B](l: List[A])(mapper: A => B):List[B] = l match
      case Cons(head,tail) => flatMap(l)(v=>Cons(mapper(v),Nil()))
      case _ => Nil()

    // Non funzionante
    def filterTwo[A](l1: List[A])(pred: A => Boolean): List[A] =
      ???

    // Non funzionante
    def max(l: List[Int]): Option[Int] =
      ???

    // Non funzionante
    def getCourses(l1: List[Person]): List[String] =
      ???

    // Non funzionante
    def foldLeft[A](list: List[A])(init: A)(f: (A,A) => A): A =
      ???

    // Non funzionante
    def foldRight[A](list: List[A])(init: A)(f: (A,A) => A): A =
      ???
    

  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60

  import List.*

  println(sum(map(filter(l)(_ >= 20))(_ + 1))) // 21+31 = 52
  println(drop(l,1))
  println(drop(l,5))

  println(append(l,Cons(40,Cons(50,Nil()))))
  println(append(Nil(),Cons(10,Nil())))
  println(flatMap(l)(v => Cons(v+1,Nil())))
  println(flatMap(l)(v => Cons(v+1,Cons(v+2,Nil()))))
  
  println(flatMap(l)(v=>Cons(v,Nil())))
  println(flatMap(l)(v=>Cons(v+1,Nil())))

  println(flatMap(l)(v=>Cons(v,Nil())))
  println(mapTwo(l)(v=>v))

