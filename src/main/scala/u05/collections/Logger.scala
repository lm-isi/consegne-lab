package u05.collections

// encapsulating the API, using an immutable var
class Logger(private val emitter: String => Unit) {
  private var strings = Seq[String]() // List
  def log(string: String): Unit = strings = strings :+ string
  def getLog: Iterable[String] = strings
  def emit(): Unit = strings.foreach(emitter)
}

// encapsulating the API, using a mutable val
class Logger2(private val emitter: String => Unit) {
  private val strings = collection.mutable.ListBuffer[String]()
  def log(string: String): Unit = strings.addOne(string)
  def getLog: Iterable[String] = strings.view // a lazy, immutable wrapper
  def emit(): Unit = strings.foreach(emitter)
}

// directly exposing the API.. a bad idea
class Logger3() extends scala.collection.mutable.ListBuffer[String]

object TryLogger extends App : // which one is better??
  val logger = new Logger( s => println("log: "+s) )
  logger.log("str1")
  logger.log("str2")
  logger.log("str3")
  logger.getLog foreach (println(_))
  logger.emit()

  val logger3 = new Logger3()
  logger3 += "str1"
  logger3 += "str2"
  logger3 += "str3"
  logger3 foreach (println(_))
